package main

import (
    "os"
    "net/http"
)

func main() {
    http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
        w.Header().Set("Content-Type", "text/plain; charset=UTF-8")
        w.Write([]byte("こんにちわ世界"))
    })
    http.ListenAndServe(os.Getenv("IP") + ":" + os.Getenv("PORT"), nil)
}
